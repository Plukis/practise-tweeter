<?php
session_start();
if (!(isset($_SESSION['udanarejestracja']) && $_SESSION['udanarejestracja']==true)){
    header('Location: index.php');
    exit();	
}	
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" href="main.css" type="text/css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <title>Thanks for joining</title>
</head>
<body>
    <div class="container">
    <h1><div class="goodInfo">Thanks for joining us, You can <a href="index.php">Sign In Now</a>.</div></h1>
    </div>
</body>
</html>