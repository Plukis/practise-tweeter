<?php
session_start();
require_once "config.php";
require_once "src/User.php";
if (isset($_POST['email'])){
    $all_good = true;
    $nick = $_POST['nick'];
    if(strlen($nick)<3 || strlen($nick)>20){
	$all_good = false;
        $_SESSION['e_newNick'] = "Nick must have between 3 and 20 char!";
    }
    if (ctype_alnum($nick) == false){
	$all_good = false;
	$_SESSION['e_newNick'] = "Name must be just from letters without special signs";
    }
    $email = $_POST['email'];
    $emailB = filter_var($email,FILTER_SANITIZE_EMAIL);
    if((filter_var($emailB, FILTER_VALIDATE_EMAIL)==false) || ($emailB != $email)){
	$all_good = false;
	$_SESSION['e_newEmail'] = "Please type correct email address";	
    }
    $haslo1 = $_POST['haslo1']; $haslo2 = $_POST['haslo2'];
    if(strlen($haslo1)<8 || strlen($haslo1)>20){
	$all_good = false;
	$_SESSION['e_haslo'] = "Password must have between 8 and 20 chars";
    }
    if($haslo1 != $haslo2){
	$all_good = false;
	$_SESSION['e_haslo'] = "Passwords must be the same";	
    }
    if(!isset($_POST['regulamin'])){
	$all_good  = false;
	$_SESSION['e_regulamin'] = "Accept policy? ";
    }
    $sekret="6LcbRG0UAAAAABRje7a2elQBevqsR1H95Wv12pDo";
    $sprawdz=file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$sekret.'&response='.$_POST['g-recaptcha-response']);
    $odpowiedz = json_decode($sprawdz);
    if($odpowiedz->success == false){
	$all_good = false;
	$_SESSION['e_bot'] = "Are You a human?";
    } 
    mysqli_report(MYSQLI_REPORT_STRICT);
    try{
	$conn = new mysqli($host,$db_user,$db_password,$db_name);
	if($conn->connect_errno!=0){
            throw new Exception(mysqli_connect_errno());
	}elseif (User::isEmailTaken($conn, $email) && 
                 User::isNameTaken($conn, $nick) &&
                 $all_good){
		$user = new User();
		$user->setUserName($nick);
		$user->setPassword($haslo1);
		$user->setEmail($email);
		$user->saveToDb($conn);
		$_SESSION['udanarejestracja']=true;
		header('Location: welcome.php');
        }else{
            throw new Exception('Please check the details again.');
        }
	$conn->close();
    }catch(Exception $e){
	echo '<div class="badInfo">Server error , sorry mate. Try again later</div>';
        }
}
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" href="main.css" type="text/css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <title>Twitter - Register</title>
</head>
<body>
    <div class="container">
	<form method="post">
            Nickname: <br /> <input type="text" name="nick" 
            <?php if (isset($_POST['nick'])){
		echo 'value="'.$_POST['nick'].'"';
            }
            ?> /><br/><br/>
            <?php if (isset($_SESSION['e_newNick'])){
                echo '<div class="error">'.$_SESSION['e_newNick'].'</div>';
		unset($_SESSION['e_newNick']);
            }
	    ?>
            E-mail: <br /> <input type="text" name="email" 
            <?php   if (isset($_POST['email'])){
                        echo 'value="'.$_POST['email'].'"';
		    }
            ?> /><br/><br/>
            <?php   if (isset($_SESSION['e_newEmail'])){
                        echo '<div class="error">'.$_SESSION['e_newEmail'].'</div>';
                        unset($_SESSION['e_newEmail']);
                    }
            ?>
            Password: <br /> <input type="password" name="haslo1" /> <br /><br />
            <?php   if (isset($_SESSION['e_haslo'])){
			echo '<div class="error">'.$_SESSION['e_haslo'].'</div>';
			unset($_SESSION['e_haslo']);
                    }	
            ?>
            Repeat password: <br /> <input type="password" name="haslo2" /> <br/><br/>
            <label><input type="checkbox" name="regulamin"/>Accept the policy? </label>
            <?php   if (isset($_SESSION['e_regulamin'])){
			echo '<div class="error">'.$_SESSION['e_regulamin'].'</div>';
			unset($_SESSION['e_regulamin']);
                    }
            ?> <br/><br/>
            <div class="g-recaptcha" data-sitekey="6LcbRG0UAAAAAJ4sZiSnBU3gHz0WPG6nPYBImLhW"></div>
            <?php   if (isset($_SESSION['e_bot'])){
                        echo '<div class="error">'.$_SESSION['e_bot'].'</div>';
			unset($_SESSION['e_bot']);
                    }
            ?> <br/>
            <input type="submit" value="Register">
	</form>
    </div>
</body>
</html>