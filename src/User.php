<?php

class User
{
    
    /**
     * @var int 
     */
    private $id;
    
    /**
     * @var string
     */
    private $userName;
    
    /**
     * @var string
     */
    private $hashedPassword;
    
    /**
     * @var string
     */
    private $email;
    
    public function __construct()
    {
	$this->id = -1;
	$this->userName = "";
        $this->hashedPassword = "";
	$this->email = "";
    }

    /**
     * @param string $newPassword
     */
    public function setPassword($newPassword)
    {
	$newHashedPassword = password_hash($newPassword, PASSWORD_BCRYPT);
	$this->hashedPassword = $newHashedPassword;
    }
    
    /**
     * @param string $newUserName
     */
    public function setUserName($newUserName)
    {
        $this->userName = $newUserName;
    }
    
    /**
     * @param string $newEmail
     */
    public function setEmail($newEmail)
    {
	$this->email = $newEmail;
    }
    
    /**
     * @return string
     */
    public function getUserName()
    {
	return $this->userName;
    }
    
    /**
     * @return string
     */
    public function getPassword()
    {	
	return $this->hashedPassword;
    }
    
    /**
     * @return string
     */
    public function getEmail()
    {
	return $this->email;
    }
    
    /**
     * @return int
     */
    public function getId()
    {
	return $this->id;
    }
    
    /**
     * @param mysqli $connection
     * @return bool
     */
    public function saveToDb(mysqli $connection)
    {	
        if ($this->id == -1){
            $sql = "INSERT INTO users(username, email, password) VALUES ('$this->userName', '$this->email', '$this->hashedPassword')";
            $result = $connection->query($sql);
            if ($result){
                $this->id = $connection->insert_id;
                return true;	
            }
        } else {
            $sql = "UPDATE users SET username='$this->userName',email='$this->email',password='$this->hashedPassword' WHERE id=$this->id";
            $result = $connection->query($sql);
            if ($result)
		return true;	
	}
        
	return false;
    }
    
    /**
     * @param mysqli $conn
     * @param int $id
     * @return User||null
     */
    //@CR tabulacja na kod znajdujący się w metodzie
    public static function loadUserById(mysqli $conn, $id)
    {
	$sql = "SELECT * FROM users WHERE id=$id";
	$result = $conn->query($sql);
            if ($result == true && $result->num_rows == 1 ){
                $row = $result->fetch_assoc();
                $loadedUser = new User();
                $loadedUser->id = $row['id'];
                $loadedUser->userName = $row['username'];
                $loadedUser->hashedPassword = $row['password'];
                $loadedUser->email = $row['email'];
                return ($loadedUser);	
            }
        
        return null;
    }
    
    /**
     * @param mysqli $connection
     * @return array||null
     */
    public static function loadAllUsers(mysqli $connection)
    {
        $sql = "SELECT * FROM users";
	$tab = [];
	$result = $connection->query($sql);
            if ($result == true && $result->num_rows != 0){
                foreach ($result as $row){
                    $loadeduser = new User;
                    $loadeduser->id = $row['id'];
                    $loadeduser->userName = $row['username'];
                    $loadeduser->hashedPassword = $row['password'];
                    $loadeduser->email = $row['email'];
                    $tab[] = $loadeduser;
                    }

                return $tab;
            }

        return null;
    }
    
    /** 
     * @param mysqli $conn
     * @return bool
     */
    public function removeUser(mysqli $conn)
    {
            if ($this->id != -1){
                $sql = "DELETE FROM users WHERE id=$this->id";
                $result = $conn->query($sql);
                if ($result){
                    $this->id = -1;
                    return true;
                }
                return false;
            }

        return true;	
    }
    
    /**
     * @param mysqli $conn
     * @param string $email
     * @return type
     */
    public static function loadUserByEmail(mysqli $conn, $email)
    {
        $sql = "SELECT * FROM users WHERE email='$email'";
	$result = $conn->query($sql);
            if ($result){
                $row = $result->fetch_assoc();
                $_SESSION['loadedUser'] = new User();
                $_SESSION['loadedUser']->id = $row['id'];
                $_SESSION['loadedUser']->userName = $row['username'];
                $_SESSION['loadedUser']->hashedPassword = $row['password']; 
                $_SESSION['loadedUser']->email = $row['email'];

                return ($_SESSION['loadedUser']);	
            }
        
	return null;
    }
    
    /**
     * @param string $nick
     * @param string $email
     * @return bool
     */
    public static function isGood($nick,$email)
    {
        $emailB = filter_var($email,FILTER_SANITIZE_EMAIL);
            if (strlen($nick) <5 || strlen($nick) >20 || empty($nick)){ 
                $_SESSION['e_newNick'] = '<div class="badInfo">Nick must be longer then 5char and shorter then 20char long</div>';

                return false;
            }
            if (ctype_alnum($nick) == false){ 
                $_SESSION['e_newNick'] = '<div class="badInfo">Nick must have just normal charackters</div>';

                return false;
            }
            if((filter_var($emailB, FILTER_VALIDATE_EMAIL)==false) || ($emailB != $email)  || empty($email)){
                $_SESSION['e_newEmail'] = '<div class="badInfo">Give the correct email please</div>';

                return false;
            }
        
        return true;
    }
    
    /**
     * @param mysqli $conn
     * @return array||null
     */
    public static function loadAllComments(mysqli $conn)
    {
	$sql = "SELECT comments.idkomment, comments.content FROM comments";
	$result = $conn->query($sql);
            if ($result == true){
                $task = $conn->query($sql);		
                $tab = [];
                foreach ($task as $row){		
                    $tab[] = $row['content'];
                }

                return $tab;
            }
        
        return null;
    }
    
    /**
     * @param string $word
     * @return bool
     */
    public static function securityCheck($word)
    {
        $wordB = filter_var($word,FILTER_SANITIZE_STRING);
            if (empty($word)){
                $_SESSION['e_newNick'] = '<div class="badInfo">The fields can not be empty</div>';

                return false;
            } 
            if ($wordB != $word){
                $_SESSION['e_newNick'] = '<div class="badInfo">Give the correct characters pls</div>';

                return false;
            }
        
        return true;
    }
    
    /**
     * @param mysqli $conn
     * @param string $name
     * @return bool
     * @throws Exception
     */
    public static function doesExist(mysqli $conn,$name)
    {
	$conn->real_escape_string($name);
	$sql = "SELECT id FROM users WHERE username='$name'";
	$result = $conn->query($sql);
            if(!$result) throw new Exception($conn->error);
            
	$nametaken = $result->num_rows;
            if($nametaken == 0){
                $_SESSION['e_newNick']='<br/><div class="badInfo">Given name '.$name.' is not registered<br/></div>';

                return false;
            }
        
        return true;
    }
    
    /**
     * @param mysqli $conn
     * @return int||null;
     */
    public function howManyComents(mysqli $conn)
    {
        $sql = "SELECT * FROM comments WHERE user_id=$this->id";
        $result = $conn->query($sql);
            if ($result) {
                return ($result->num_rows);	
            }
        
	return null;
    }
    
    /**
     * @param mysqli $conn
     * @return int||null;
     */
    public function howManyPosts($conn)
    {
	$sql = "SELECT * FROM wpis WHERE iduser=$this->id";
	$result = $conn->query($sql);
            if ($result){	
                return ($result->num_rows);
            }
        
        return null;	
    }
    
    /**
     * @param mysqli $conn
     * @param string $email
     * @return bool
     */
    public function isEmailTaken(mysqli $conn,$email)
    {
        $email = filter_var($email,FILTER_SANITIZE_EMAIL);
        $sql = "SELECT id FROM users WHERE email='$email'";
        $result = $conn->query($sql);
            if($result->num_rows > 0){
                $_SESSION['e_newEmail']='<div class="badInfo">This email is already taken</div><br/>';

                return false;
            }

        return true;
    }
    
    /**
     * @param mysqli $conn
     * @param string $name
     * @return bool
     */
    public function isNameTaken(mysqli $conn,$name)
    {
        $name = $conn->real_escape_string($name);	
	$sql = "SELECT id FROM users WHERE username='$name'";
	$result = $conn->query($sql);
            if($result->num_rows > 0){
                $_SESSION['e_newNick']='<div class="badInfo">This name is already taken</div><br/>';

                return false;
            }
        
        return true;
    }
    
}