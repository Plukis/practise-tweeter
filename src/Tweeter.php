<?php

class Tweeter
{

    /**
     * @var int
     */
    private $idPost;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $creationDate;

    /**
     * @var int
     */
    private $idComment;

    /**
     * @var string
     */
    private $topic;

    public function __construct()
    {
        $this->idPost = -1;
        $this->userId = "";
        $this->text = "";
        $this->creationDate = "";
        $this->idComment = "";
        $this->topic = "";
    }

    /**
     * @return int
     */
    public function getIdPost()
    {
        return $this->idPost;
    }

    /**
     * @param int $idPost
     */
    public function setIdPost($idPost)
    {
        $this->idPost = $idPost;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param string $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return int
     */
    public function getIdComment()
    {
        return $this->idComment;
    }

    /**
     * @param int $idComment
     */
    public function setIdComment($idComment)
    {
        $this->idComment = $idComment;
    }

    /**
     * @return string
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param string $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @param mysqli $conn
     * @param int $idWpis
     * @return Tweeter|null
     */
    public static function loadTweetById(mysqli $conn, $idWpis)
    {
        $sql = "SELECT * FROM wpis WHERE idwpis=$idWpis";
        $result = $conn->query($sql);
            if ($result == true && $result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $loadedTWEET = new Tweeter();
                $loadedTWEET->idPost = $row['idwpis'];
                $loadedTWEET->userId = $row['iduser'];
                $loadedTWEET->text = $row['zawartosc'];
                $loadedTWEET->creationDate = $row['data'];
                $loadedTWEET->revile($conn);

                return $loadedTWEET;
            }

        return null;
    }

    /**
     * @param mysqli $connection
     * @return bool
     */
    public function savePosttoDB(mysqli $connection)
    {
        if ($this->idPost == -1) {
            $sql = "INSERT INTO wpis(zawartosc, iduser, data) VALUES ('$this->text', '$this->userId', '$this->creationDate')";
            $result = $connection->query($sql);
            if ($result) {
                $this->idPost = $connection->insert_id;

                return true;
            }
        } else {
            $sql = "UPDATE wpis SET zawartosc='$this->text',iduser='$this->userId',data='$this->creationDate' WHERE idwpis = $this->id";
            $result = $connection->query($sql);
            if ($result) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param mysqli $conn
     * @param int $idUser
     * @return array|null
     */
    public static function loadAllUserTweet(mysqli $conn, $idUser)
    {
        $idUser = (int)$idUser;
        $sql = "SELECT * FROM wpis WHERE iduser=$idUser ORDER BY data DESC";
        $tab = [];
        $result = $conn->query($sql);
            if ($result && $result->num_rows > 0) {
                foreach ($result as $row) {
                    $loadedTweet = new Tweeter();
                    $loadedTweet->idPost = $row['idwpis'];
                    $loadedTweet->userId = $row['iduser'];
                    $loadedTweet->text = $row['zawartosc'];
                    $loadedTweet->creationDate = $row['data'];
                    $tab[] = $loadedTweet;
                }

                return $tab;
            }

        return null;
    }

    /**
     * @param mysqli $connection
     * @return bool
     */
    public function saveIdKoment(mysqli $connection)
    {
        $connection->real_escape_string($this->text);
        $sql = "INSERT INTO comments(user_id, topic_id,content) VALUES ('$this->userId', '$this->idComment','$this->text')";
        $result = $connection->query($sql);
            if ($result) {
                return true;
            }

        return false;
    }


    /**
     * @param mysqli $conn
     * @param int $idUser
     * @return array|null
     */
    public static function loadAllUserTweetByID(mysqli $conn, $idUser)
    {
        $idUser = (int)$idUser;
        $sql = "SELECT * FROM wpis WHERE iduser=$idUser ORDER BY data DESC";
        $tab = [];
        $result = $conn->query($sql);
            if ($result && $result->num_rows > 0) {
                foreach ($result as $row) {
                    $loadedTweet = new Tweeter();
                    $loadedTweet->idPost = $row['idwpis'];
                    $loadedTweet->userId = $row['iduser'];
                    $loadedTweet->text = $row['zawartosc'];
                    $loadedTweet->creationDate = $row['data'];
                    $tab[] = $loadedTweet;
                }

                return $tab;
            }

        return null;
    }

    /**
     * @param mysqli $conn
     * @param int $idPost
     * @return array|null
     */
    public static function findComment(mysqli $conn, $idPost)
    {
        $sql = "SELECT comments.content,comments.user_id,comments.topic_id FROM comments,wpis WHERE idwpis=$idPost 
                    AND comments.topic_id=wpis.idwpis ORDER BY idkomment DESC";
        $result = $conn->query($sql);
        $tab = [];
            if ($result && $result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $foundedComment = new Tweeter();
                    $foundedComment->text = $row['content'];
                    $foundedComment->userId = $row['user_id'];
                    $foundedComment->topic = $row['topic_id'];
                    $tab[] = $foundedComment;
                }

                return $tab;
            }

        return null;
    }

    /**
     * @param mysqli $conn
     * @param int $idUser
     * @return string|null
     */
    public static function findNameOfComment(mysqli $conn, $idUser)
    {
        $sql = "SELECT users.username FROM users WHERE id=$idUser";
        $result = $conn->query($sql);
            if ($result) {
                $row = $result->fetch_assoc();
                $userName = $row['username'];

                return $userName;
            }

        return null;
    }

    /**
     * @param mysqli $conn
     * @return array
     */
    public function revile(mysqli $conn)
    {
        $tab = [];
        $sql = "SELECT zawartosc,idwpis,iduser,data FROM `wpis` order by data DESC LIMIT 3";
        $result = $conn->query($sql);
            while ($row = $result->fetch_assoc()) {
                $showNewTweet = new Tweeter();
                $showNewTweet->creationDate = $row['data'];
                $showNewTweet->idPost = $row['idwpis'];
                $showNewTweet->text = $row['zawartosc'];
                $showNewTweet->userId = $row['iduser'];
                $tab[] = $showNewTweet;
            }

            return $tab;
    }

}