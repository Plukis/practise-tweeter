<?php

class Message
{
    
    /**
     * @var int
     */
    private $idMss;
    
    /**
     * @var int
     */
    private $senderId; 
    
    /**
     * @var int
     */
    private $reciverId;
    
    /**
     * @var string 
     */
    private $creationDate;
    
    /**
     * @var string
     */
    private $text;
    
    /**
     * @var string
     */
    private $subject;
    
    /**
     * @var string 
     */
    private $lastReply;
    
    /**
     * @var int
     */
    private $unRead;
    
    public function __construct()
    {
	$this->idMss = "";
	$this->senderId = "";
	$this->reciverId = "";
	$this->creationDate = "";
        $this->text = "";
        $this->subject = "";
        $this->lastReply = "";
        $this->unRead = "";
    }
    
    /**
     * @param int $idMss
     */
    public function setIdMss($idMss)
    {
	$this->idMss = $idMss;
    }
    
    /**
     * @param int $senderId
     */
    public function setSenderId($senderId)
    {
	$this->senderId = $senderId;
    }
    
    /**
     * @param int $reciverId
     */
    public function setReciverId($reciverId)
    {
	$this->reciverId = $reciverId;
    }
    
    /**
     * @param string $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }
    
    /**
     * @param string $text
     */
    public function setText($text)
    {
	$this->text = $text;
    }
    
    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
	$this->subject = $subject;
    }
    
    /**
     * @param string $lastReply
     */
    public function setLastReply($lastReply)
    {
        $this->lastReply = $lastReply;
    }
    
    /**
     * @param int $unRead
     */
    public function setUnRead($unRead)
    {
        $this->unRead = $unRead;
    }
    
    /**
     * @return string
     */
    public function getLastReply()
    {
        return $this->lastReply;
    }
    
    /**
     * @return int
     */
    public function getUnRead()
    {
        return $this->unRead;
    }
    
    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }
    
    /**
     * @return int
     */
    public function getIdMss()
    {
        return $this->idMss;
    }
    
    /**
     * @return int
     */
    public function getSenderId()
    {
	return $this->senderId;
    }
    
    /**
     * @return string
     */
    public function getText()
    {
	return $this->text;
    }
    
    /**
     * @param mysqli $conn
     * @param string $name
     * @return int||null
     */
    public static function loadUserIdByName(mysqli $conn, $name)
    {
	$conn->real_escape_string($name);	
	$sql = "SELECT id FROM users WHERE username=('$name')";
	$result = $conn->query($sql);
            if ($result && $result->num_rows>0 ) {
                $row = $result->fetch_assoc();
                $_SESSION['Loadedid'] = $row['id'];

                return ($_SESSION['Loadedid']);		
            }
        
	return null;
    }	
    
    /**
     * @param mysqli $conn
     * @param int $dialogId
     * @param int $idUser
     * @return bool
     */
    public static function validateID($conn, $dialogId , $idUser)
    {		
        $dialogId = (int)$dialogId;
        $sql = "SELECT COUNT(1) FROM dialog_members WHERE dialog_id = $dialogId AND user_id = $idUser AND dialog_removed = 0";
        $result = $conn->query($sql);
            if ($result->num_rows == 0){
               return false;
            }
            
        return true;		
    }
    
    /**
     * @param mysqli $conn
     * return bool
     */
    public function createConversation($conn)
    {
	$sub = $conn->real_escape_string($this->text);
	$text = $conn->real_escape_string($this->subject);
	$sql = "INSERT INTO dialog(subject) VALUES ('$sub')";
	$result1 = $conn->query($sql);
            if (!$result1) {
                return false;
            }
	$dialog_id = $conn->insert_id;	
	$sql2 = "INSERT INTO
		dialog_messages(dialog_id, user_id, dialog_date, dialog_text) 
		VALUES ('$dialog_id', '$this->senderId', now(), '$text')";
	$result2 = $conn->query($sql2);
            if (!$result2) {
                return false;
            }
	$sql3 = "INSERT INTO
		dialog_members(dialog_id, user_id, dialog_seen, dialog_removed) 
		VALUES('$dialog_id','$this->reciverId',0,0)";
	$result3 = $conn->query($sql3);
            if (!$result3) {
                return false;
            }
	$sql4 = "INSERT INTO
         	dialog_members(dialog_id, user_id, dialog_seen, dialog_removed) 
		VALUES('$dialog_id','$this->senderId',now(),0)";
	$result4 = $conn->query($sql4);
            if (!$result4) {
                return false;
            }
        
        return true;
    }
    
    /**
     * @param mysqli $conn
     * @param int $iduser
     * @return Message
     */
    public function findAndShow($conn, $idUser)
    {
	$sql = "SELECT dialog.dialog_ID,
                dialog.subject,
		MAX(dialog_messages.dialog_date) as lastreply, MAX(dialog_messages.dialog_date) > (dialog_members.dialog_seen) as unread FROM dialog, dialog_messages
		INNER JOIN dialog_members WHERE dialog.dialog_ID = dialog_messages.dialog_id
		AND dialog.dialog_ID = dialog_members.dialog_id AND dialog_members.user_id = $idUser 
		AND dialog_members.dialog_removed = 0
		GROUP BY dialog.dialog_ID ORDER BY lastreply DESC";		
	$result = $conn->query($sql);
	$dialogs = [];
            while($row = $result->fetch_assoc()){	
                $foundedDialog = new Message();
                $foundedDialog->idMss = $row['dialog_ID'];
                $foundedDialog->subject = $row['subject'];
                $foundedDialog->lastReply = $row['lastreply'];
                $foundedDialog->unRead = ($row['unread'] == 1);     
                $dialogs[] = $foundedDialog;
            }
        
	return $dialogs;	
    }
    
    /**
     * @param mysqli $conn
     * @return bool
     */
    public function addDialogMessage($conn)
    {
        $this->text = $conn->real_escape_string(htmlentities($this->text));
        $sql = "INSERT INTO dialog_messages
               (dialog_ID, user_id, dialog_date, dialog_text) 
                VALUES ('$this->idMss', '$this->senderId', now(), '$this->text')";
        $result = $conn->query($sql);
            if ($result) {
                return true;
            }
        
        return false;
    }
    
    /**
     * @param mysqli $conn
     * @param int $dialogId
     * @param int $idUser
     * @return bool
     */
    public static function updateDialogLastView($conn, $dialogId, $idUser)
    {
        $dialogId = (int)$dialogId;
        $sql = "UPDATE dialog_members
                SET dialog_seen = now()
                WHERE dialog_id = $dialogId
                AND user_id = $idUser;";
        $result = $conn->query($sql);
            if ($result) {
                return true;
            }
        
        return false;
    }
    
    /**
     * 
     * @param mysqli $conn
     * @param int $dialogId
     * @param int $userId
     * @return Message||null
     */
    public static function findDialogMessages($conn, $dialogId, $userId)
    {
        $dialogId = (int)$dialogId;
        $sql = "SELECT dialog_messages.dialog_date,
                dialog_messages.dialog_date > dialog_members.dialog_seen as unseen,
                dialog_messages.dialog_text, users.username FROM dialog_messages
                INNER JOIN users ON dialog_messages.user_id = users.id
                INNER JOIN dialog_members ON dialog_messages.dialog_id = dialog_members.dialog_id
                WHERE dialog_messages.dialog_id = $dialogId
                AND dialog_members.user_id = $userId
                ORDER BY dialog_messages.dialog_date DESC";
        $result = $conn->query($sql);
            if ($result) {
                $messages = [];
                while($row = $result->fetch_assoc()){
                    $dialogMessage = new Message();
                    $dialogMessage->lastReply = $row['dialog_date'];
                    $dialogMessage->text = $row['dialog_text'];
                    $dialogMessage->senderId = $row['username'];
                    $dialogMessage->unRead = $row['unseen'];
                    $messages[] = $dialogMessage;
                }
                return $messages;
            }
        
        return null;
    }
    
    /**
     * @param mysqli $conn
     * @param int $dialogId
     * @param int $userId
     * @return bool
     */
    public static function deleteDialog($conn, $dialogId, $userId)
    {
        $dialogId = (int)$dialogId;
        $sql="SELECT dialog_removed 
              FROM dialog_members 
              WHERE user_id != $userId
              AND dialog_id=$dialogId";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
        $isOne = $row['dialog_removed'];
            if ($result->num_rows==1 && $isOne=="1"){
                $conn->query("DELETE FROM dialog WHERE dialog_id=$dialogId");
                $conn->query("DELETE FROM dialog_members WHERE dialog_id=$dialogId");
                $conn->query("DELETE FROM dialog_messages WHERE dialog_id=$dialogId");

                return true;

            } elseif ($result->num_rows==1 && $isOne=="0"){
                $sql = "UPDATE dialog_members SET dialog_removed = 1
                        WHERE dialog_id=$dialogId AND user_id = $userId";
                $conn->query($sql);

                return true;
            }
        
        return false;
    }   
}	