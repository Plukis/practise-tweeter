<?php
session_start();
if (!isset($_SESSION['loggedIn'])){
    header('Location: index.php');
    exit();
}
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>	
    <link rel="stylesheet" href="main.css" type="text/css" />
    <title>Witamy uzytkownika</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <div class="container">
	<div class="messagebox2">
	<h5>Message Box</h5><a href="inbox.php">[inbox]</a><br/>
	<a href="index.php">[mainpage]</a><br/>
	<a href="logout.php">[logout]</a><br/>
	</div>	
	<?php
        require_once('src/User.php');
        require_once('src/Tweeter.php');	
	require_once('src/Message.php');	
	require_once('config.php');
        
	if (isset($_POST['to'], $_POST['subject'], $_POST['text'])
            && User::securityCheck($_POST['to'])
            && User::securityCheck($_POST['subject'])
            && User::securityCheck($_POST['text'])){
            try{			
                $conn = new mysqli($host,$db_user,$db_password,$db_name);
                if($conn->connect_errno!=0){
                    throw new Exception(mysqli_connect_errno());
		} elseif (User::doesExist($conn,$_POST['to'])){
                    $user = User::loadUserByEmail($conn, $_SESSION['email']);
                    $reciverId = Message::loadUserIdByName($conn, $_POST['to']);
                    $senderId = $user->getId();
                    $wiadomosc = new Message();
                    $wiadomosc->setReciverId($reciverId);
                    $wiadomosc->setSenderId($senderId);
                    $wiadomosc->setText($_POST['text']);
                    $wiadomosc->setSubject($_POST['subject']);
                    $wiadomosc->createConversation($conn);
                    $_SESSION['wasSent'] = '<div class="greenbox">Your message was sent</div>';
                    $conn->close();
                    header('Location: inbox.php');
                    exit();
		} else {
                    echo '<div class="badInfo">Please check crenencials</div>';
                }
                $conn->close();					
            } catch (Exception $e) {
		echo '<div class="badInfo">There was a problem , sorry mate. Try again later</div>';
		}
        }
	?>
	<form method="POST">
            <label for="to">To: </label>
            <input type="text" name="to" id="to"/>
            <label for="subject">Subject: </label>
            <input type="text" name="subject" id="subject" />
            <textarea name="text" rows="20" cols="110"></textarea></br>
            <input type="submit" value="SEND" />
            <?php
            if (isset($_SESSION['e_newNick'])){
		echo $_SESSION['e_newNick'];
		unset($_SESSION['e_newNick']);
            }
            ?>
	</form>
    </div>
</body>
</html>