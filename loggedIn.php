<?php
session_start();
if (!isset($_SESSION['loggedIn'])){
    header('Location: index.php');
    exit();
}
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="main.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <div class="container">
	<div class="messagebox">
	    <h4>Message Box</h4><a href="inbox.php">[Inbox]</a><br/>
	    <a href="newConversation.php">[NewMessage]</a><br/>
	</div>
	<?php
	require_once('src/User.php');	
	require_once('src/Tweeter.php');	
	require_once('config.php');
        
	try{			
            $conn = new mysqli($host,$db_user,$db_password,$db_name);
            if($conn->connect_errno!=0){
		throw new Exception(mysqli_connect_errno());
            }else{
		if (isset($_SESSION['email'])){
                    echo "<b>Welcome, ";
                    $user = User::loadUserByEmail($conn, $_SESSION['email']);
                    echo $user->getUserName()."</b><br/>";
                    ?>			
                    Do You want to add new post?<br/><br/>
                    <form method="POST">
                        <textarea name="nextPost" cols="70" rows="3" maxlength="160"class="newpost"></textarea>
                        <br/><input type="submit" value="Add post">
                    </form>	
                    <?php			
                    echo "<br/><b>SEE THE NEWEST POSTS<br/></b>";
                    $twit = Tweeter::revile($conn);
                    foreach($twit as $newTwitt){ 
                        $userinfo = User::loadUserById($conn, $newTwitt->getUserId());
                        echo "<br/>Author: ";
                        echo '<a href="detailsAboutMe.php?who='.$userinfo->getUserName().'" id="black" title="Mydetalis" class="black">'.$userinfo->getUserName().'</a>';
                        echo '<table class="tabOne" border="1" cellspacing="0">
                        <tr> <td>'.$newTwitt->getText()."</td> <br/> <td>".$newTwitt->getCreationDate()."</td></tr>
                        </table>";
                        echo '<form method="POST">
                        <input type="hidden" name="addComment" value="'.$newTwitt->getIdPost().'"/><br/>
                        <textarea name="content" cols="70" rows="3" maxlength="60"></textarea>
                        <br/>
                        <input type="submit" id="guzik" value="AddKoment"/>
                        </form>
                        <h2> COMMENTS</h2>';
                        if ($koment = Tweeter::findComment($conn, $newTwitt->getIdPost())){
                            foreach($koment as $writeKoment){
                                $userName = Tweeter::findNameOfComment($conn, $writeKoment->getUserId());
                                echo '<a href="detailsAboutMe.php?who='.$userName.'"><b>'.$userName.'</b></a>';
                                echo "<br/>".$writeKoment->getText()."<br/>"; 
                            }
                        }
                    }
                    if (isset($_POST['addComment'])){
                        $text = $conn->real_escape_string($_POST['content']);
                        $WhereTo = $_POST['addComment'];
                        $nowyTwitter = new Tweeter();
                        $nowyTwitter->setUserId($user->getId());
                        $nowyTwitter->setText($text);
                        $nowyTwitter->setIdComment($WhereTo);
                        $nowyTwitter->saveIdKoment($conn);
                        unset($_POST['addComment']);
                        header('Refresh:0');
                    }	
                    if (isset($_POST['nextPost']) && !empty($_POST['nextPost'])){
                        $text = $_POST['nextPost'];
                        $nowyTwitter = new Tweeter();
                        $nowyTwitter->setText($text);
                        $nowyTwitter->setCreationDate(date('Y-m-d H:i:s'));
                        $nowyTwitter->setUserId($user->getId());
                        $nowyTwitter->savePosttoDB($conn);
                        unset($_POST['nextPost']);
                        header('refresh:0');
                    }
                    if (isset($_POST['Remove'])){
                        $_SESSION['loadedUser']->removeUser($conn);
                        header('refresh:2;url=logout.php');
                    }          
		}
		$conn->close();
            }	
	} catch (Exception $e) {
            echo '<div class="badInfo">Server Error , sorry mate. Try again later</div>';
            }	
	?>        
	<br/>Finished posts.<br/>
	<div id="finish">	
            <h3><br/>Would You like to modify Your acc?</h3>
            <a href="modify.php"><div class="goodInfo">[MODIFY]</div></a><br/>
            <form method="POST">
		<input type="hidden" name="Remove">
		<input type="submit" value="[DELETE]">
            </form>
            <a href ="logout.php"><div class="goodInfo"><br/>[LOGOUT]<br/></div></a>	
        </div>		
    </div>
</body>
</html>