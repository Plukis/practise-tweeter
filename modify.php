<?php
session_start();
if (!isset($_SESSION['loggedIn'])){
    header('Location: index.php');
    exit();
}	
require_once('src/User.php');	
require_once('config.php');

if(isset($_POST['newnick']) && isset($_POST['newemail'])){				
    if(User::isGood($_POST['newnick'],$_POST['newemail'])){
            $conn = new mysqli($host,$db_user,$db_password,$db_name);
            if($conn->connect_errno!=0){
            throw new Exception(mysqli_connect_errno());
            }else{
		User::loadUserByEmail($conn, $_SESSION['email']);			
		$newnick = $_POST['newnick'];
		$newemail = $_POST['newemail'];
		$newpass = $_POST['newpassword'];
		if(User::isEmailTaken($conn,$newemail) && 
                   User::isNameTaken( $conn, $newnick)){
                    $_SESSION['loadedUser']->setPassword($newpass);
                    $_SESSION['loadedUser']->setEmail($newemail);
                    $_SESSION['loadedUser']->setUserName($newnick);
                    $_SESSION['loadedUser']->saveToDb($conn);
                    $_SESSION['changes'] = '<div class="goodInfo">Changes saved successfully , log in again please </div>';
                    $conn->close();
                    header('location: logout.php');
		}
		$conn->close();	
            }		
    }
}
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>	
    <link rel="stylesheet" href="main.css" type="text/css" />
    <title>Change Your Account</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <div class="container">
	<h1>Modify your account</h1>
	<form method="POST" action="modify.php">
            <br/>New Nick :<br/><input type="text" name="newnick"><br/>
            <?php
            if (isset($_SESSION['e_newNick'])){
		echo $_SESSION['e_newNick'];
		unset($_SESSION['e_newNick']);
		}
            ?>					
            <br/>New Email :<br/><input type="email" name="newemail"><br/>
            <?php
            if (isset($_SESSION['e_newEmail'])){
		echo $_SESSION['e_newEmail'];
		unset($_SESSION['e_newEmail']);
            }
            ?>					
            <br/>Insert new password:<br/><input type="password" name="newpassword">
            <br/><input type="submit" value="Done">				
        </form>
	</br><a href ="logout.php"><div class="badInfo"><br/>[LOGOUT]<br/></div></a>
    </div>
</body>
</html>