<?php
session_start();
if (!isset($_SESSION['loggedIn'])){
    header('Location: index.php');
    exit();
}
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="main.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ALL ABOUT ME</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <div class="container">
	<div class="messagebox">
            <h4>Message Box</h4><a href="inbox.php">[Inbox]</a><br/>
            <a href="newConversation.php">[NewMessage]</a><br/>
	</div>
	<?php
	require_once('src/User.php');	
	require_once('src/Tweeter.php');	
	require_once('config.php');
	require_once('src/Message.php');
        
	try{			
            $conn = new mysqli($host,$db_user,$db_password,$db_name);
            if($conn->connect_errno!=0){
		throw new Exception(mysqli_connect_errno());
            } elseif (isset($_GET['who']) && 
		    User::doesExist($conn,$_GET['who'])){
                        $name = $_GET['who'];
                        $conn->real_escape_string($name);
                        $hisId = Message::loadUserIdByName($conn, $name);
                        $user = User::loadUserById($conn, $hisId);
                        echo "<b>USERNAME:</b> ".$user->getUserName()."<br/>";
                        echo "<b>EMAIL:</b> ".$user->getEmail()."</b><br/>";
                        echo "<b>NB OF COMMENTS:</b> ".$user->howManyComents($conn)."<br/>";
                        echo "<b>NB OF POSTS:</b> ".$user->howManyPosts($conn)."<br/>";
                        echo "<b>ID:</b> ".$user->getId()."</b><br/>";
                        echo '<a href="newConversation.php"><b>SEND A MESSAGE</b> <br/></a>';
                        echo '<div id="koniec">';
                        $yourTweets = Tweeter::loadAllUserTweetByID($conn,$user->getId() );
                        unset($_GET['who']);
                        foreach($yourTweets as $newTwitt){
                            echo "<br/>Author: ";
                            echo '<a href="detailsAboutMe.php?who='.$user->getUserName().'" id="black" title="Mydetalis">'.$user->getUserName().'</a>';
                            echo '<table class="tabOne" border="1" cellspacing="0">
                            <tr> <td>'.$newTwitt->getText()."</td> <br/> <td>".$newTwitt->getCreationDate()."</td></tr>
                            </table>";
                            echo '<form method="POST">
                            <input type="hidden" name="dodajKoment" value="'.$newTwitt->getIdPost().'"/><br/>
                            <textarea name="KolejneZad" cols="70" rows="3" maxlength="60"></textarea>
                            <br/>
                            <input type="submit" id="guzik" value="AddKoment"/>
                            </form>
                            <h2> COMMENTS</h2>';
                            if ($koment = Tweeter::findComment($conn, $newTwitt->getIdPost())){
                                foreach($koment as $writeKoment){
                                    $userName = Tweeter::findNameOfComment($conn, $writeKoment->getUserId());
                                    echo '<a href="detailsAboutMe.php?who='.$userName.'"><b>'.$userName.'</b></a>';
                                    echo "<br/>".$writeKoment->getText()."<br/>"; 
                                }
                            }
                        } 
                        echo '</div>';           
                } else {
                    echo '<div class="badInfo">There was a problem , check again later</div>';
                }
		$conn->close();		
	} catch(Exception $e) {
		echo '<div class="badInfo">server error , sorry mate. Try again later</div>';
		}
	?>
    </div>
</body>
</html>