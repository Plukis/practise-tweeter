<?php
session_start();
if (isset($_SESSION['loggedIn'])){
    header('Location: loggedIn.php');
    exit();
}
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="main.css" type="text/css" />
    <title>Welcome</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <div class="container">
	<h1>Please log in</h1>
    <?php
	require_once('src/User.php');	
	require_once('config.php');
        
	if (isset($_POST['email']) &&
            $_POST['email'] != ""  && 
            isset($_POST['pass'])){
            mysqli_report(MYSQLI_REPORT_STRICT);
            try{
		$conn = new mysqli($host,$db_user,$db_password,$db_name);
		if ($conn->connect_errno!=0){
                    throw new Exception(mysqli_connect_errno());
		}else{
                    $email = filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
                    $password = $conn->real_escape_string($_POST['pass']);		
                    $result = $conn->query("SELECT * FROM users WHERE email='$email'");
                    if (!$result){ 
			throw new Exception($conn->error);
                        exit();
			}
                    $isOneThere = $result->num_rows;
                    if ($isOneThere>0){
			$wiersz = $result->fetch_assoc();
			if (password_verify($password, $wiersz['password'])){
                            $_SESSION['email'] = $wiersz['email'];
                            $_SESSION['loggedIn'] = true;
                            $result->free_result();
                            header('Location: LoggedIn.php');
			} else {
                            $_SESSION['zleHaslo'] = '<div class="badInfo"><br/>Given Password is   wrong, try again</div>';	
                            }	
                    } else {
			$_SESSION['zlyEmail'] = '<div class="badInfo"><br/>Given Email is not registered</div>';
			}	
                    $conn->close();	
                }
            } catch(Exception $e) {
		echo '<div class="badInfo">blad servera , sorry mate. Try again later</div>'; 
		}
	}	
		?>
        <form method="POST">
            Email<br/><input type="email" name="email">
            <?php
            if (isset($_SESSION['zlyEmail'])){
		echo $_SESSION['zlyEmail'];
		unset($_SESSION['zlyEmail']);
            }
            ?>
            <br/>Password<br/><input type="password" name="pass">	
            <?php
            if (isset($_SESSION['zleHaslo'])){
		echo $_SESSION['zleHaslo'];
		unset($_SESSION['zleHaslo']);
            }
            ?>
            <br/><input type="submit" value="Log In"><br/><br/>
	</form>
	Want to become a member? 
	<a href="register.php"><br/>[Join]<br/></a>
    </div>
</body>
</html>