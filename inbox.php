<?php
session_start();
if (!isset($_SESSION['loggedIn'])){
    header('Location: index.php');
    exit();
}
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="main.css" type="text/css" />
    <title>INBOX</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <div class="container">		
    <?php
    if(isset($_SESSION['wasSent'])){
	echo $_SESSION['wasSent'];
	unset($_SESSION['wasSent']);
    }
    ?>
        Welcome to Your inbox
        <br/><br/>
        <div class="conversation">
            <a href="newConversation.php">[New conversation]</a>
            <a href="logout.php">[Logout]</a><br/>
	</div>
	<?php
	require_once('src/User.php');
	require_once('src/Message.php');
	require_once('src/Tweeter.php');	
	require_once('config.php');
        
	try{			
            $conn = new mysqli($host,$db_user,$db_password,$db_name);
            if($conn->connect_errno!=0){
		throw new Exception(mysqli_connect_errno());
            } else {
            $user = User::loadUserByEmail($conn, $_SESSION['email']);
            $error = array();
            if(isset($_GET['deleteDialog'])){
		if(Message::validateID($conn,$_GET['deleteDialog'] , $user->getId()) == false){
		$error[] = "Invalid conversation ID";
		}
		if(empty($error)){
                    Message::deleteDialog($conn, $_GET['deleteDialog'], $user->getId());
		}
            }
            $letSee = Message::findAndShow($conn, $user->getId());
            if (empty($letSee)){
            $error[] = '<h2><div class="badInfo">No messages</div></h2>';
            }
            if (!empty($error)){
		foreach ($error as $err){
		echo '<div class="badInfo">'.$err.'</div><br/>';
		}
            }
            foreach($letSee as $dialog){	
            ?>
            <div class="conversation <?php if($dialog->getUnRead()){
                echo "unread";} ?>">
                <a href="inbox.php?deleteDialog=<?php 
                echo $dialog->getIdMss();?>">[DEL]</a>
            <a href="viewConversation.php?dialog_id=<?php echo $dialog->getIdMss();?>"> 
                <?php echo $dialog->getSubject(); ?></a>
            <p>Last Reply: <?php echo $dialog->getLastReply();?></p>
            </div>
            <?php
                }	
            }
            $conn->close();	
	} catch (Exception $e) {
            echo '<div class="badInfo">blad servera , sorry mate. Try again later</div>'; 
            }
	?>
            <div class="messagebox2">
		<h5>Message Box</h5><a href="index.php">[Mainpage]</a><br/>
		<a href="newConversation.php">[Newmessage]</a><br/>
		<a href="logout.php">[Logout]</a><br/>
            </div>
    </div>
</body>
</html>