-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2019 at 02:04 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `twitter-dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `idkomment` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `content` varchar(180) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`idkomment`, `user_id`, `topic_id`, `content`) VALUES
(37, 12, 35, 'Example Comment nb 1'),
(38, 13, 35, 'Example reply to a comment nb 2'),
(39, 13, 36, 'Example comment nb 1'),
(40, 13, 36, 'Example nb2'),
(41, 13, 34, 'Example nb 3'),
(42, 13, 36, 'Example 4');

-- --------------------------------------------------------

--
-- Table structure for table `dialog`
--

CREATE TABLE `dialog` (
  `dialog_ID` int(10) UNSIGNED NOT NULL,
  `subject` varchar(120) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `dialog`
--

INSERT INTO `dialog` (`dialog_ID`, `subject`) VALUES
(69, 'ExampleMessage to user-Example2');

-- --------------------------------------------------------

--
-- Table structure for table `dialog_members`
--

CREATE TABLE `dialog_members` (
  `dialog_id` int(8) NOT NULL,
  `user_id` int(8) NOT NULL,
  `dialog_seen` datetime NOT NULL,
  `dialog_removed` tinyint(1) NOT NULL,
  `cokolwiek` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `dialog_members`
--

INSERT INTO `dialog_members` (`dialog_id`, `user_id`, `dialog_seen`, `dialog_removed`, `cokolwiek`) VALUES
(69, 13, '2019-02-03 11:21:33', 0, 71),
(69, 12, '2019-02-03 11:19:30', 0, 72);

-- --------------------------------------------------------

--
-- Table structure for table `dialog_messages`
--

CREATE TABLE `dialog_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `dialog_id` int(8) NOT NULL,
  `user_id` int(8) NOT NULL,
  `dialog_date` datetime NOT NULL,
  `dialog_text` varchar(200) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `dialog_messages`
--

INSERT INTO `dialog_messages` (`message_id`, `dialog_id`, `user_id`, `dialog_date`, `dialog_text`) VALUES
(83, 69, 12, '2019-02-03 11:19:30', 'ExampleTopic'),
(84, 69, 13, '2019-02-03 11:21:33', 'Example reply to the private message');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(12, 'Example1', 'Example1@gmail.com', '$2y$10$NpjkXHvvdkC4hHI8jkAkMekOfHCN619WWqK.HqHugVcCZ2kMGtNym'),
(13, 'Example2', 'Example2@gmail.com', '$2y$10$26qA9AmKj49otlBrrseOp.0Y6AJmZ4RVsKkthIB2D/56cZt1aJIPa');

-- --------------------------------------------------------

--
-- Table structure for table `wpis`
--

CREATE TABLE `wpis` (
  `idwpis` int(10) UNSIGNED NOT NULL,
  `zawartosc` text COLLATE utf8_polish_ci NOT NULL,
  `iduser` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `email` varchar(60) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `wpis`
--

INSERT INTO `wpis` (`idwpis`, `zawartosc`, `iduser`, `data`, `email`) VALUES
(34, 'Example Post nb 1', 12, '2019-02-03 12:18:27', ''),
(35, 'Example Post nb 2', 12, '2019-02-03 12:18:38', ''),
(36, 'Example Post nb 3', 13, '2019-02-03 12:19:55', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`idkomment`);

--
-- Indexes for table `dialog`
--
ALTER TABLE `dialog`
  ADD PRIMARY KEY (`dialog_ID`);
ALTER TABLE `dialog` ADD FULLTEXT KEY `subject` (`subject`);

--
-- Indexes for table `dialog_members`
--
ALTER TABLE `dialog_members`
  ADD PRIMARY KEY (`cokolwiek`);

--
-- Indexes for table `dialog_messages`
--
ALTER TABLE `dialog_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `wpis`
--
ALTER TABLE `wpis`
  ADD PRIMARY KEY (`idwpis`);
ALTER TABLE `wpis` ADD FULLTEXT KEY `zawartosc` (`zawartosc`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `idkomment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `dialog`
--
ALTER TABLE `dialog`
  MODIFY `dialog_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `dialog_members`
--
ALTER TABLE `dialog_members`
  MODIFY `cokolwiek` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `dialog_messages`
--
ALTER TABLE `dialog_messages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wpis`
--
ALTER TABLE `wpis`
  MODIFY `idwpis` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
