<?php
session_start();
if (!isset($_SESSION['loggedIn'])){
    header('Location: index.php');
    exit();
}
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="main.css" type="text/css" />
    <title>View</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <div class="container">	
    <?php
	require_once('src/User.php');
	require_once('src/Message.php');
	require_once('src/Tweeter.php');	
	require_once('config.php');
	try{			
            $conn = new mysqli($host,$db_user,$db_password,$db_name);
            if($conn->connect_errno!=0){
                throw new Exception(mysqli_connect_errno());
            }else{
            $user = User::loadUserByEmail($conn, $_SESSION['email']);
            $error = [];
            $valid_dialog = (isset($_GET['dialog_id']) && 
			     Message::validateID($conn, $_GET['dialog_id'], $user->getId()));
            if ($valid_dialog == false){
		$error[] = "Invalid dialog ID.";
            }	
            if (isset($_POST['message'])){
		if (empty($_POST['message'])){
                    $error[] = "You must enter a message";
		}
		if (empty($error)){
                    $wiadomosc = new Message();
                    $wiadomosc->setIdMss($_GET['dialog_id']);
                    $wiadomosc->setText($_POST['message']);
                    $wiadomosc->setSenderId($user->getId());
                    $wiadomosc->addDialogMessage($conn);
		}
            }
            if (!empty($error)){
		foreach ($error as $err){
                    echo '<div class="badInfo">'.$err.'</div><br/>';
		}
            }
            if($valid_dialog){
		if (isset($_POST['message'])){
                    Message::updateDialogLastView($conn, $_GET['dialog_id'], $user->getId());
                    $messages = Message::findDialogMessages($conn, $_GET['dialog_id'], $user->getId());
		} else {
                    $messages = Message::findDialogMessages($conn, $_GET['dialog_id'], $user->getId());
                    Message::updateDialogLastView($conn, $_GET['dialog_id'], $user->getId());
                }
                unset($_POST['message']);
                ?>	
		View Your conversation<br/><br/>
		<div class="conversation">
	            <a href="inbox.php">[BackToInbox]</a>
                    <a href="logout.php">[Logout]</a><br/>
		</div>
                <?php	
		foreach ($messages as $mssg){
                    ?>
                    <div class="conversation <?php if($mssg->getUnRead()){
                            echo "unread";}?>">
                            <?php echo $mssg->getSenderId()."(".$mssg->getLastReply().")<br/>".$mssg->getText(); ?>	
                    </div>	
                    <?php
		}
		?>
		<br/>
		<form method="POST">
                    <textarea name="message" rows="6" cols="60"></textarea><br/>
                    <input type="submit" value="Reply"/>
                </form><br/>
		<?php
		}				
            $conn->close();				
            }					
	} catch (Exception $e) {
            echo '<div class="badInfo">blad servera , sorry mate. Try again later</div>';
            }	
    ?>
    </div>
</body>
</html>