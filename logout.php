<?php
session_start();
if (!isset($_SESSION['loggedIn'])){
    header('Location: index.php');
    exit();
}
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="main.css" type="text/css" />
    <title>Thanks for Your time</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <div class="container">
	<h1>Logged out successfully</h1>
	<?php
	if (isset($_SESSION['changes'])){
            echo $_SESSION['changes'];
	}
        session_unset();
	?>
	<a href ="index.php"><div class="badInfo"><br/>[Back]<br/></div></a>
    </div>
</body>
</html>